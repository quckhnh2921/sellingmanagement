﻿USE master
GO

DROP DATABASE IF EXISTS SellingManagement
GO

CREATE DATABASE SellingManagement
GO

USE SellingManagement
GO

CREATE TABLE KhachHang
(
	MaKH INT PRIMARY KEY IDENTITY (1,1),
	HoTen NVARCHAR(50),
	SoDT VARCHAR(10),
	NgaySinh DATETIME,
	DoanhSo MONEY,
	NgayDK DATETIME
)

CREATE TABLE NhanVien
(
	MaNV INT PRIMARY KEY IDENTITY (1,1),
	HoTen NVARCHAR(50),
	SoDT VARCHAR(10),
	NgayVaoLam DATETIME
)

CREATE TABLE SanPham
(
	MaSP INT PRIMARY KEY IDENTITY(1,1),
	TenSP NVARCHAR(50),
	DVT NVARCHAR(10),
	NuocSX NVARCHAR(50),
	Gia MONEY
)

CREATE TABLE HoaDon
(
	SoHD INT PRIMARY KEY IDENTITY (1,1),
	NgayHD DATETIME,
	MaKH INT FOREIGN KEY REFERENCES KhachHang(MaKH),
	MaNV INT FOREIGN KEY REFERENCES NhanVien(MaNV),
	TriGia MONEY
)

CREATE TABLE CTHD 
(
	SoHD INT FOREIGN KEY REFERENCES HoaDon(SoHD) NOT NULL,
	MaSP INT FOREIGN KEY REFERENCES SanPham(MaSP) NOT NULL,
	SoLuong INT
)

ALTER TABLE CTHD ADD PRIMARY KEY (SoHD, MaSP)

--INSERT KHACH HANG
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Frederique Skpsey', '5241307931', '01/28/1991', '$614.06', '2/28/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Dalt Pinfold', '8752915985', '02/08/2001', '$539.72', '1/21/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Gay McKissack', '6888068799', '08/17/1992', '$760.45', '7/19/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Earle Somes', '4743996806', '05/17/1993', '$712.08', '9/15/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Zackariah Lattimore', '1104840375', '08/06/1997', '$970.20', '10/16/2022');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Angie Schutter', '9907269256', '07/13/1996', '$660.99', '12/27/2022');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Abbe Wathey', '5434142528', '08/15/1992', '$560.76', '1/6/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Fran Elis', '4707037241', '02/01/1997', '$722.97', '4/8/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Milton Battaille', '7713671113', '12/08/1993', '$970.64', '8/4/2023');
insert into KhachHang (HoTen, SoDT, NgaySinh, DoanhSo, NgayDK) values ('Rodolphe Ander', '7429319838', '01/10/1992', '$995.32', '8/8/2023');

--INSERT NHAN VIEN
insert into NhanVien (HoTen, SoDT, NgayVaoLam) values ('Akim Masdin', '5756277845', '04/16/2021');
insert into NhanVien (HoTen, SoDT, NgayVaoLam) values ('Neil Alenshev', '5025150612', '12/08/2021');
insert into NhanVien (HoTen, SoDT, NgayVaoLam) values ('Collette Crees', '1357228948', '12/31/2022');
insert into NhanVien (HoTen, SoDT, NgayVaoLam) values ('Nanon Ingerith', '4401866587', '06/27/2022');
insert into NhanVien (HoTen, SoDT, NgayVaoLam) values ('Arni Heale', '8439119896', '08/30/2021');

--INSERT SAN PHAM
insert into SanPham (TenSP, DVT, NuocSX, Gia) values ('Ecolab - Balanced Fusion', 'Kg', 'China', '$348.50');
insert into SanPham (TenSP, DVT, NuocSX, Gia) values ('Raspberries - Frozen', 'Kg', 'Viet Nam', '$217.31');
insert into SanPham (TenSP, DVT, NuocSX, Gia) values ('Wiberg Super Cure', 'Kg', 'Greece', '$446.00');
insert into SanPham (TenSP, DVT, NuocSX, Gia) values ('Capers - Pickled', 'Kg', 'United States', '$135.77');
insert into SanPham (TenSP, DVT, NuocSX, Gia) values ('Pork - Smoked Back Bacon', 'Kg', 'United States', '$330.71');


--INSERT HOA DON
insert into HoaDon (NgayHD, MaKH, MaNV, TriGia) values ('1/17/2023', 10, 2, '$141.30');
insert into HoaDon (NgayHD, MaKH, MaNV, TriGia) values ('10/29/2022', 3, 4, '$314.23');
insert into HoaDon (NgayHD, MaKH, MaNV, TriGia) values ('2/19/2023', 3, 5, '$185.88');
insert into HoaDon (NgayHD, MaKH, MaNV, TriGia) values ('3/2/2023', 10, 2, '$328.25');
insert into HoaDon (NgayHD, MaKH, MaNV, TriGia) values ('8/3/2023', 3, 1, '$399.84');

SELECT *FROM HoaDon

--INSERT CTHD
insert into CTHD (SoHD, MaSP, SoLuong) values (1, 2, 5);
insert into CTHD (SoHD, MaSP, SoLuong) values (2, 5, 5);
insert into CTHD (SoHD, MaSP, SoLuong) values (3, 4, 10);
insert into CTHD (SoHD, MaSP, SoLuong) values (4, 3, 6);
insert into CTHD (SoHD, MaSP, SoLuong) values (5, 2, 6);

--Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất hoặc các sản phẩm bán trong ngày 23/05/2023
SELECT s.TenSP, s.NuocSX, hd.NgayHD
FROM SanPham s
INNER JOIN CTHD ct ON s.MaSP = ct.MaSP
INNER JOIN HoaDon hd ON hd.SoHD = ct.SoHD
WHERE s.NuocSX = 'Viet Nam' OR hd.NgayHD = '05/23/2023'  
--Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được
SELECT s.MaSP, s.TenSP
FROM SanPham s
LEFT JOIN CTHD ct ON ct.MaSP = s.MaSP
WHERE ct.MaSP IS NULL
--Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất
SELECT hd.SoHD
FROM HoaDon hd
INNER JOIN CTHD ct ON hd.SoHD = ct.SoHD
INNER JOIN SanPham s ON s.MaSP = ct.MaSP
WHERE s.NuocSX = 'Viet Nam'


